sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/ui/model/json/JSONModel"
], function (Controller, JSONModel, FilterOperator) {
    "use strict";

    return Controller.extend("sap.ui.putsykovich.alexei.controller.Start", {
        onInit : function () {
        },
        onOrderPress: function (oEvent) {
            // get the source control of event object (the one that was fired event)
            var oSource = oEvent.getSource();

            // get the binding context of a button (it's a part of the table line, so it inherits the context of it)
            var oCtx = oSource.getBindingContext("odata");


            // get the component
            var oComponent = this.getOwnerComponent();
            oComponent.getRouter().navTo("secondPage", {
                orderId: oCtx.getObject("id")
            });
        }
    });
});

