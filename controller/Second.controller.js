sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/ui/core/routing/History"
], function (Controller, History,ODataModel) {
    "use strict";
    return Controller.extend("sap.ui.putsykovich.alexei.controller.Second", {
        onInit:function () {
            var oComponent=this.getOwnerComponent();
            var oRouter=oComponent.getRouter();
            oRouter.getRoute("secondPage").attachPatternMatched(this.onPatternMatched,this);

        },
        onPatternMatched:function (oEvent) {
          var that=this;
          var mRouteArguments=oEvent.getParameter("arguments");
          var sOrderID=mRouteArguments.orderId;
          var oODataModel=this.getView().getModel("odata");
          oODataModel.metadataLoaded().then(function () {
              var sKey=oODataModel.createKey("/Orders",{id:sOrderID});
              that.getView().bindObject({
                  path:sKey,
                  model:"odata"
              });
          });
        },
        onProductPress: function (oEvent) {
            // get the source control of event object (the one that was fired event)
            var oSource = oEvent.getSource();

            // get the binding context of a button (it's a part of the table line, so it inherits the context of it)
            var oCtx = oSource.getBindingContext("odata");


            // get the component
            var oComponent = this.getOwnerComponent();
            oComponent.getRouter().navTo("thirdPage", {
                productId: oCtx.getObject("id")
            });
        },

        onNavBack: function () {
            var oHistory = History.getInstance();
            var sPreviousHash = oHistory.getPreviousHash();

            if (sPreviousHash !== undefined) {
                window.history.go(-1);
            } else {
                var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
                oRouter.navTo("overview", {}, true);
            }
        }
    });
});