sap.ui.define([
		"sap/ui/core/mvc/Controller",
		"sap/ui/core/routing/History"
	], function (Controller, History,ODataModel) {
		"use strict";

		return Controller.extend("sap.ui.putsykovich.alexei.controller.Third", {
            onInit: function () {
                var oComponent = this.getOwnerComponent();

                var oRouter = oComponent.getRouter();

                oRouter.getRoute("thirdPage").attachPatternMatched(this.onPatternMatched, this);
            },
            onPatternMatched: function (oEvent) {
                var that = this;

                var mRouteArguments = oEvent.getParameter("arguments");

                var sProductID = mRouteArguments.productId;

                var oODataModel = this.getView().getModel("odata");

                oODataModel.metadataLoaded().then(function () {

                    var sKey = oODataModel.createKey("/OrderProducts", {id: sProductID});

                    that.getView().bindObject({
                        path: sKey,
                        model: "odata"
                    });
                });

            },
			onNavBack: function () {
				var oHistory = History.getInstance();
				var sPreviousHash = oHistory.getPreviousHash();

				if (sPreviousHash !== undefined) {
					window.history.go(-1);
				} else {
					var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
					oRouter.navTo("detail", {}, true);
				}
			}
		});
	}
);
